/*
 * app.js
 * Ludique.js Demo Platformer
 *
 * Recreation of Super Mario Bros. World 1-1 to demonstrate Ludique.js features
 * All game assets owned by Nintendo
 * By Raphaël Pion
 */

import { game } from './src/game.js'
import { screen } from './src/screen.js'

const app = {
  name: 'Ludique.js Demo Platformer',
  version: '1.0',
  size: { x: 256, y: 240 },
  init: () => {
    console.log(`🕹️ Running ${app.name} version ${app.version}`)

    // initialize the screen and start the game
    screen.init()
    game.init()
  },
}

window.addEventListener('load', app.init)

export { app }
