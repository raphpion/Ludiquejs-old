/*
 *  marioAnimations.js
 *  Animations for Mario
 *
 *  by Raphaël Pion
 */

import { Animation } from '../../src/classes/animation.js'
import { MarioSprites } from '../sprite/marioSprites.js'

const MarioAnimations = {
  idle: {
    right: new Animation([MarioSprites.idle.right], 1000),
    left: new Animation([MarioSprites.idle.left], 1000),
  },
  jump: {
    right: new Animation([MarioSprites.jump.right], 1000),
    left: new Animation([MarioSprites.jump.left], 1000),
  },
  walk: {
    right: new Animation([MarioSprites.walk.right[0], MarioSprites.walk.right[1], MarioSprites.walk.right[2]], 100),
    left: new Animation([MarioSprites.walk.left[0], MarioSprites.walk.left[1], MarioSprites.walk.left[2]], 100),
  },
}

export { MarioAnimations }
