/*
 *  marioSprites.js
 *  Sprites for Mario
 *
 *  by Raphaël Pion
 */

import { Sprite } from '../../src/classes/sprite.js'
import { MarioLuigiSheet } from '../spritesheet/marioLuigiSheet.js'

const MarioSprites = {
  idle: {
    right: new Sprite(MarioLuigiSheet, 0, 0, 16, 16),
    left: new Sprite(MarioLuigiSheet, 0, 16, 16, 16),
  },
  jump: {
    right: new Sprite(MarioLuigiSheet, 0, 64, 16, 16),
    left: new Sprite(MarioLuigiSheet, 16, 64, 16, 16),
  },
  walk: {
    right: [new Sprite(MarioLuigiSheet, 16, 0, 16, 16), new Sprite(MarioLuigiSheet, 32, 0, 16, 16), new Sprite(MarioLuigiSheet, 48, 0, 16, 16)],
    left: [new Sprite(MarioLuigiSheet, 16, 16, 16, 16), new Sprite(MarioLuigiSheet, 32, 16, 16, 16), new Sprite(MarioLuigiSheet, 48, 16, 16, 16)],
  },
}

export { MarioSprites }
