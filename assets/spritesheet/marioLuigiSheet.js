/*
 *  marioLuigiSheet.js
 *  Spritesheet for Mario and Luigi
 *
 *  by Raphaël Pion
 */

import { SpriteSheet } from '../../src/classes/spritesheet.js'

const MarioLuigiSheet = new SpriteSheet('assets/spritesheet/mario-luigi.png')

export { MarioLuigiSheet }
