/*
 * game.js
 * Manage the game loop, move and animate elements
 *
 * By Raphaël Pion
 */

import { screen } from './screen.js'
import { MarioSprites } from '../assets/sprite/marioSprites.js'
import { MarioAnimations } from '../assets/animation/marioAnimations.js'

const game = {
  paused: false,
  draw: () => {
    // move all the game objects then draw
    // if (game.paused) don't move objects
    Mario.draw()
  },
  init: () => {
    // initialize the scene
    screen.currentScene = game

    // initialize the game objects
  },
  pause: () => {
    // toggle paused state
    game.paused = game.paused ? false : true

    // pause all game objects animations

    // show pause menu
  },
}

const Mario = {
  sprite: MarioSprites.idle.right,
  pos: { x: 0, y: 0 },
  draw: () => {
    Mario.sprite.draw(Mario.pos.x, Mario.pos.y)
  },
}

export { game }
