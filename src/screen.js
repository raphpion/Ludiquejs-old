/*
 * screen.js
 * Manage the screen canvas, load scenes, draw debug
 *
 * By Raphaël Pion
 */

import { app } from '../app.js'

const screen = {
  canvas: undefined,
  ctx: undefined,
  debug: false,
  scale: undefined,
  dt: 0, // last frame duration in milliseconds
  st: 0, // last frame start timestamp in milliseconds
  currentScene: null,
  draw: t => {
    // calculate time elapsed
    screen.dt = t - screen.st
    screen.st = t

    // clear canvas and draw current scene
    screen.ctx.clearRect(0, 0, screen.canvas.width, screen.canvas.height)
    if (screen.currentScene) screen.currentScene.draw()

    // draw debug screen
    if (screen.debug) screen.drawDebug()

    // continue the loop
    requestAnimationFrame(screen.draw)
  },
  drawDebug: () => {
    // text style
    screen.ctx.font = '8px monospace'
    screen.ctx.fillStyle = '#00ff44'
    screen.ctx.textBaseline = 'top'

    // app & debug information
    screen.ctx.textAlign = 'left'
    screen.ctx.fillText(`${app.name} v${app.version}`, 5, 5)
    screen.ctx.fillText(`Press 'u' to toggle debug`, 5, 15)

    // frames per second
    screen.ctx.textAlign = 'right'
    let fps = Math.floor(1000 / screen.dt)
    screen.ctx.fillText(`${fps} FPS`, screen.canvas.width / screen.scale - 5, 5)
  },
  init: () => {
    // get page elements & context
    screen.wrapper = document.getElementById('screen-wrapper')
    screen.canvas = document.getElementById('screen')
    screen.ctx = screen.canvas.getContext('2d')

    // resize the screen and add event on resize
    screen.resize()
    window.onresize = screen.resize

    // start the screen drawing loop
    requestAnimationFrame(screen.draw)
  },
  resize: () => {
    // get maximum screen scale where min = 1
    let wrapperSize = { x: screen.wrapper.offsetWidth, y: screen.wrapper.offsetHeight }
    let maxScale = {
      x: Math.floor(wrapperSize.x / app.size.x),
      y: Math.floor(wrapperSize.y / app.size.y),
    }
    if (maxScale.x < 1 || maxScale.y < 1) screen.scale = 1
    else screen.scale = maxScale.x >= maxScale.y ? maxScale.y : maxScale.x

    // resize the screen
    screen.canvas.width = screen.scale * app.size.x
    screen.canvas.height = screen.scale * app.size.y

    // scale the screen
    screen.ctx.scale(screen.scale, screen.scale)

    //? disable smoothing (not working...)
    screen.ctx.webkitImageSmoothingEnabled = false
    screen.ctx.mozImageSmoothingEnabled = false
    screen.ctx.imageSmoothingEnabled = false
  },
}

export { screen }
